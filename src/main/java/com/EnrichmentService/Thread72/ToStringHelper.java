package com.EnrichmentService.Thread72;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ToStringHelper {
	private static final String QUOTATION = "\"";
	private static final String MEMBER_SEPARATOR = ", ";
	private static final int MEMBER_SEPARATOR_LEN = MEMBER_SEPARATOR.length();

	private enum Prefix {
		GET("get"), IS("is"), HAS("has");

		private final String prefixName;
		private final int prefixNameLength;

		private Prefix(final String prefixName) {
			this.prefixName = prefixName;
			prefixNameLength = prefixName.length();
		}

		public String getPrefixName() {
			return prefixName;
		}

		public int getPrefixNameLength() {
			return prefixNameLength;
		}
	}

	public static String toString(final Object o) {
		return ToStringHelper.toString(o, null);
	}

	public static String toString(final Object o, final String resolvePackage) {
		StringBuilder sb = new StringBuilder();
		if (o == null) {
			return "null";
		} else if (o.getClass().isArray()) {
			sb.append("[");
			for (Object i : (Object[])o) {
				sb.append(ToStringHelper.toString(i, resolvePackage));
			}
			sb.append("]");
		} else {
			sb.append("{");

			boolean foundMethods = false;
			for (Method m : o.getClass().getMethods()) {
				if (isConvertibleMethod(m)) {
					try {
						Object object = m.invoke(o);
						// write property name
						sb.append(QUOTATION).append(getProperty(m.getName())).append("\":");
						// write property value
						if (isResolveObject(object, resolvePackage)) {
							// if we want to resolve the child object(s), add them
							// via recursion
							sb.append(ToStringHelper.toString(object, resolvePackage));
						} else {
							// add the simple values / call object.toString() and
							// append this
							boolean quotationNeeded = isQuotationNeeded(object);
							if (quotationNeeded) {
								sb.append(QUOTATION);
							}
							sb.append(object);
							if (quotationNeeded) {
								sb.append(QUOTATION);
							}
						}
						sb.append(MEMBER_SEPARATOR);
						foundMethods = true;
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
			// remove the ", " from the last method
			if (foundMethods) {
				sb.replace(sb.length() - MEMBER_SEPARATOR_LEN, sb.length(), "");
			}
			sb.append("}");
		}
		return sb.toString();
	}

	private static boolean isConvertibleMethod(final Method m) {
		return m.getParameterCount() == 0 && hasValidPrefix(m.getName());
	}

	public static boolean hasValidPrefix(final String methodName) {
		if (methodName != null) {
			for (Prefix item : Prefix.values()) {
				if (methodName.startsWith(item.getPrefixName())) {
					return true;
				}
			}
		}
		return false;
	}

	private static String getProperty(final String methodName) {
		StringBuilder sb = new StringBuilder();
		if (methodName != null) {
			int start = 0;
			for (Prefix item : Prefix.values()) {
				if (methodName.startsWith(item.getPrefixName())) {
					start = item.getPrefixNameLength();
					break;
				}
			}
			sb.append(Character.toLowerCase(methodName.charAt(start)) + methodName.substring(start + 1));
		}
		return sb.toString();
	}

	private static boolean isQuotationNeeded(final Object result) {
		return result != null && !(result instanceof Boolean) && !(result instanceof Integer)
				&& !(result instanceof Float) && !(result instanceof Double);
	}

	private static boolean isResolveObject(final Object obj, final String resolvePackage) {
		boolean result = obj != null && resolvePackage != null &&
				obj.getClass() != null &&
				obj.getClass().getCanonicalName() != null
				&& obj.getClass().getCanonicalName().contains(resolvePackage);
		return result;
	}
}
