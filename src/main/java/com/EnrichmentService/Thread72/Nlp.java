package com.EnrichmentService.Thread72;

public class Nlp {
	public Nlp() {
	}

	private String mainCategory;

	public String getMainCategory() {
		return mainCategory;
	}

	public void setMainCategory(final String mainCategory) {
		this.mainCategory = mainCategory;
	}

	@Override
	public String toString() {
		return ToStringHelper.toString(this,
				this.getClass().getPackage().getName());
	}
}