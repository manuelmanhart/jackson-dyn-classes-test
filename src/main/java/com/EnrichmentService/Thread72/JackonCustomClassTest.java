package com.EnrichmentService.Thread72;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JackonCustomClassTest {
	private static final String TARGET_CLASSES_DIR = "./target/classes";
	private static final String NLP_CLASS = "Nlp";
	private static final String CATEGORY_CLASS = "Category";
	public static String PACKAGE_NAME = "com.EnrichmentService.Thread72";
	public static String MAIN_CLASS = "EsRootDoc";
	public static String CANONICAL_NAME = PACKAGE_NAME + "." + MAIN_CLASS;

	public static void main(final String args[]) throws Exception {

		System.out.println("Reload classed via TestClassloader... ");
		generateTestClassesV1();
		System.out.println(JackonCustomClassTest.class.getResource(".").toString());
		runJackson(getFileContent("test1.json"), new TestClassLoader().loadClass(CANONICAL_NAME));

		generateTestClassesV2();
		runJackson(getFileContent("test2.json"), new TestClassLoader().loadClass(CANONICAL_NAME));

		System.out.println("Not working via default classloader...");
		generateTestClassesV1();
		runJackson(getFileContent("test1.json"), Class.forName(CANONICAL_NAME));

		generateTestClassesV2();
		runJackson(getFileContent("test2.json"), Class.forName(CANONICAL_NAME));
	}

	private static String getFileContent(final String name) {
		return getStringFromInputStream(JackonCustomClassTest.class.getResourceAsStream(name));
	}

	private static void generateTestClassesV2() throws Exception {
		// update category class
		Map<String, String> categoryMembersMap = new HashMap<>();
		categoryMembersMap.put("nlp", NLP_CLASS + "[]");
		generateAndCompileClass(PACKAGE_NAME, CATEGORY_CLASS, categoryMembersMap, PACKAGE_NAME + "." + NLP_CLASS);

		generateMainClass();
	}

	private static void generateTestClassesV1() throws Exception {
		// generate nlp class
		Map<String, String> nlpMembersMap = new HashMap<>();
		nlpMembersMap.put("mainCategory", "String");
		generateAndCompileClass(PACKAGE_NAME, NLP_CLASS, nlpMembersMap);

		// generate category class
		Map<String, String> categoryMembersMap = new HashMap<>();
		categoryMembersMap.put("nlp", NLP_CLASS);
		generateAndCompileClass(PACKAGE_NAME, CATEGORY_CLASS, categoryMembersMap, PACKAGE_NAME + "." + NLP_CLASS);

		generateMainClass();
	}

	private static void generateMainClass() throws Exception {
		// generate main class
		Map<String, String> mainMembersMap = new HashMap<>();
		mainMembersMap.put("contentLanguage", "String");
		mainMembersMap.put("contentLength", "Integer");
		mainMembersMap.put("dtStamp", "String");
		mainMembersMap.put("publicationName", "String");
		mainMembersMap.put("category", CATEGORY_CLASS + "[]");
		mainMembersMap.put("uniqueId", "String");
		generateAndCompileClass(PACKAGE_NAME, MAIN_CLASS, mainMembersMap, PACKAGE_NAME + "." + CATEGORY_CLASS);
	}

	public static void runJackson(final String json, final Class<?> clazz) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper m = new ObjectMapper();
		Object o = m.readValue(json, clazz);
		System.out.println("result of conversion: " + o.toString());
	}

	public static Class<?> compileClass(final String fullyQualifiedClassName, final String source) throws Exception {
		// Save source in .java file.
		File root = new java.io.File(TARGET_CLASSES_DIR);
		File sourceFile = new File(root, fullyQualifiedClassName.replace(".", "/") + ".java");
		sourceFile.getParentFile().mkdirs();
		Files.write(sourceFile.toPath(), source.getBytes(StandardCharsets.UTF_8));

		// Compile source file.
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		compiler.run(null, null, null, sourceFile.getPath());

		// Load and instantiate compiled class.
		// URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {
		// root.toURI().toURL() });
		// Class<?> cls = Class.forName(fullyQualifiedClassName, true,
		// classLoader);
		Class<?> cls = new TestClassLoader().loadClass(fullyQualifiedClassName);
		return cls;
	}

	static class TestClassLoader extends ClassLoader {
		@Override
		public Class<?> loadClass(final String name) throws ClassNotFoundException {
			if (name.startsWith(PACKAGE_NAME)) {
				try {
					InputStream is = this.getClass().getClassLoader()
							.getResourceAsStream(name.replace(".", "/") + ".class");
					byte[] buf = new byte[is.available()];
					int len = is.read(buf);

					Class<?> c = defineClass(name, buf, 0, len);
					resolveClass(c);
					return c;

				} catch (IOException e) {
					throw new ClassNotFoundException("", e);
				}
			}
			return getParent().loadClass(name);
		}
	}

	public static Class<?> generateAndCompileClass(final String packageName, final String className, final Map<String, String> members, final String... imports)
			throws Exception {
		String source = generateClassSource(packageName, className, members, imports);
		return compileClass(packageName + "." + className, source);
	}

	public static String generateClassSource(final String packageName, final String className, final Map<String, String> members, final String... imports) {
		StringBuilder sb = new StringBuilder();
		sb.append("package ").append(packageName).append("; ");
		if (imports != null && imports.length > 0) {
			for (String i : imports) {
				sb.append("import ").append(i).append("; ");
			}
		}
		sb.append("public class ").append(className).append(" { public ").append(className).append("() { } ");
		for (Entry<String, String> e : members.entrySet()) {
			String member = e.getKey();
			String memberFunction = member.substring(0, 1).toUpperCase() + member.substring(1);
			String type = e.getValue();
			sb.append("private " + type + " " + member + "; public " + type + " get" + memberFunction + "() { return "
					+ member + "; } public void set" + memberFunction + "(" + type + " " + member + ") { this." + member
					+ " = " + member + "; }");
		}
		sb.append("public String toString() { return ToStringHelper.toString(this, this.getClass().getPackage().getName() ); } ");
		sb.append(" } ");
		return sb.toString();
	}

	private static String getStringFromInputStream(final InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}
}
