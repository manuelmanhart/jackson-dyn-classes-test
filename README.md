# Synopsis

This is an example project of dynamically created (and recompiled) java classes put into Jacksonfor converting json objects.

# Motivation

Recreating & fixing the problem of 
https://stackoverflow.com/questions/45518549/jackson-deserialisation-typereference-for-dynamically-loaded-pojo-class

# Installation

Compile with maven, execute JackonCustomClassTest class (maybe you need to adapt the constant TARGET_CLASSES_DIR)

# Contributors

only me, feel free to fork and experiment on your own

# License

MIT